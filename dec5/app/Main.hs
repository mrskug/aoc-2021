module Main where

import Dec5

main :: IO ()
main = do
  input_raw <- readFile "input.txt"
  let input   = map (\x -> read x :: Line) (lines input_raw)
      partOne = getOverlaps input
      partTwo = getAllOverlaps input
  print (partOne, partTwo)
