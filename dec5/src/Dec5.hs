module Dec5 where

import Data.List.Utils
import Data.List

data Line = Line (Int, Int) (Int, Int)
  deriving (Show, Eq)

instance Read Line where
  readsPrec _ s = [(
    (\[x1,y1,x2,y2] -> Line (x1,y1) (x2,y2)) (map (\x -> read x :: Int) (split "," (replace "->" "," s)))
    ,"")]

lineToPoints :: Line -> [(Int,Int)]
lineToPoints (Line (x1,y1)(x2,y2))
  | (x1 /= x2) && (y1 /= y2) = mempty
  | otherwise = do
  let xRange = getRange x1 x2
      yRange = getRange y1 y2
  genPoints xRange yRange

genPoints :: [Int] -> [Int] -> [(Int,Int)]
genPoints xR yR
  | length xR == 1 = zip (replicate (length yR) (head xR)) yR
  | length yR == 1 = zip xR (replicate (length xR) (head yR))
  | otherwise      = zip xR yR

lineToAllPoints :: Line -> [(Int,Int)]
lineToAllPoints (Line (x1,y1)(x2,y2)) = do
  let xRange = getRange x1 x2
      yRange = getRange y1 y2
  genPoints xRange yRange

getRange :: Int -> Int -> [Int]
getRange a b
  | a < b     = [a..b]
  | otherwise = reverse [b..a]

frequency :: [(Int,Int)] -> [((Int,Int), Int)]
frequency xs = map (\x -> (head x, length x)) (group (sort xs))

getOverlaps :: [Line] -> Int
getOverlaps l = do
  let points = concatMap lineToPoints l
      freqs  = frequency points
  length (filter (>=2) (map snd freqs))

getAllOverlaps :: [Line] -> Int
getAllOverlaps l = do
  let points = concatMap lineToAllPoints l
      freqs  = frequency points
  length (filter (>=2) (map snd freqs))
