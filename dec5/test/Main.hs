module Main where

import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.Hspec
import qualified Test.Tasty.Runners.Reporter as Reporter

import Dec5

main :: IO ()
main = defaultMainWithIngredients [Reporter.ingredient] unitTests

lx :: [Line]
lx = [ Line (0,9) (5,9)
     , Line (8,0) (0,8)
     , Line (9,4) (3,4)
     , Line (2,2) (2,1)
     , Line (7,0) (7,4)
     , Line (6,4) (2,0)
     , Line (0,9) (2,9)
     , Line (3,4) (1,4)
     , Line (0,0) (8,8)
     , Line (5,5) (8,2)
     ]

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "Part 1" $
      getOverlaps lx `shouldBe` 5
  , testCase "Part 2" $
      getAllOverlaps lx `shouldBe` 12
  ]
