module Dec3 where

import Data.List
import Data.Char (digitToInt)

getGammaRate :: [String] -> Int
getGammaRate xs = binToInt $ map mostCommon (transpose xs)

getEpsilonRate :: [String] -> Int
getEpsilonRate xs = binToInt $ map leastCommon (transpose xs)

getOxygenRating :: [String] -> Int
getOxygenRating = binToInt . filterListWith 0 mostCommon

getCO2Rating :: [String] -> Int
getCO2Rating = binToInt . filterListWith 0 leastCommon

mostCommon :: String -> Char
mostCommon s
  | countOnes s >= (length s - countOnes s) = '1'
  | otherwise                               = '0'

leastCommon :: String -> Char
leastCommon = flipDigit . mostCommon

countOnes :: [Char] -> Int
countOnes l = length(dropWhile (=='0') (sort l))

flipDigit :: Char -> Char
flipDigit x
  | x == '0'  = '1'
  | otherwise = '0'

binToInt :: String -> Int
binToInt = foldl' (\acc x -> acc * 2 + digitToInt x ) 0

filterListWith :: Int -> (String -> Char) -> [String] -> String
filterListWith _ _ [x]   = x
filterListWith pos fn xs = filterListWith (pos + 1) fn (filter (\x -> (x !! pos) == fn (transpose xs !! pos)) xs)
