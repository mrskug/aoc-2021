module Main where

import Dec3

main :: IO ()
main = do
  input <- readFile "input.txt"
  let inputLines   = lines input
      gammaRate    = getGammaRate inputLines
      epsilonRate  = getEpsilonRate inputLines
      oxygenRating = getOxygenRating inputLines
      co2Rating    = getCO2Rating inputLines
  print $ (gammaRate * epsilonRate, oxygenRating * co2Rating)
