module Main where

import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.Hspec
import qualified Test.Tasty.Runners.Reporter as Reporter

import Dec3

main :: IO ()
main = defaultMainWithIngredients [Reporter.ingredient] unitTests

input :: [String]
input = ["00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001", "00010", "01010"]

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "Part 1" $
      (getGammaRate input, getEpsilonRate input ) `shouldBe` (22, 9)
  , testCase "Part 2" $
      (getOxygenRating input, getCO2Rating input) `shouldBe` (23,10)
  ]
