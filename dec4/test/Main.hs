module Main where

import           Data.List.Utils
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.Hspec
import qualified Test.Tasty.Runners.Reporter as Reporter

import Dec4

main :: IO ()
main = do
  numbers_raw <- readFile "testnums.txt"
  boards_raw  <- readFile "testboards.txt"
  let numbers = map (\x -> read x :: Int) $ split "," numbers_raw
      boards  = generateBoards (lines boards_raw) [[[]]]
  defaultMainWithIngredients [Reporter.ingredient] $ unitTests boards numbers



unitTests :: [Board] -> [Int] -> TestTree
unitTests boards numbers = testGroup "Unit Tests"
  [ testCase "Part 1" $
      gameLoop boards numbers `shouldBe` 4512
  , testCase "Part 2" $
      losingLoop boards [([[]],0)] numbers `shouldBe` 1924
  ]
