module Main where

import Data.List.Utils (split)

import Dec4

main :: IO ()
main = do
  numbers_raw <- readFile "numbers.txt"
  boards_raw  <- readFile "boards.txt"
  let numbers = map (\x -> read x :: Int) $ split "," numbers_raw
      boards  = generateBoards (lines boards_raw) [[[]]]
      partOne = gameLoop boards numbers
      partTwo = losingLoop boards [([[]],0)] numbers
  print (partOne, partTwo)
