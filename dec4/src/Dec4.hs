module Dec4 where

import Data.List (transpose)

data BingoNum = Checked Int | Unchecked Int
  deriving (Eq, Ord)

instance Show BingoNum where
  show (Checked _)   = show "X"
  show (Unchecked i) = show i

instance Read BingoNum where
  readsPrec _ c = [(Unchecked (read c :: Int), "")]

type BingoRow = [BingoNum]
type Board = [BingoRow]


generateBoards :: [String] -> [Board] -> [Board]
generateBoards [] xs       = tail $ reverse xs
generateBoards ("":xs) acc = generateBoards xs acc
generateBoards xs acc      = generateBoards (dropWhile (/="") xs) (map readRow (takeWhile (/="") xs):acc)

readRow :: String -> BingoRow
readRow row = map (\x -> read x :: BingoNum) $ words row

markBoards :: Int -> [Board] -> [Board]
markBoards num = map (markBoard num)

markBoard :: Int -> Board -> Board
markBoard num = map ( map (check num))

check :: Int -> BingoNum -> BingoNum
check n b = case b of
  Unchecked i -> if i == n then
                   Checked i else
                   b
  Checked _ -> b

checkBoard :: Board -> Bool
checkBoard board = do
  let hor      = map checkRow board
      ver      = map checkRow (transpose board)
      checkRow = all isChecked
  or (hor ++ ver)

isChecked :: BingoNum -> Bool
isChecked (Checked _) = True
isChecked _           = False

checkBoards :: [Board] -> Maybe Board
checkBoards [] = Nothing
checkBoards (b:bs)
  | checkBoard b = Just b
  | otherwise    = checkBoards bs

checkAllBoards :: [Board] -> [Board] -> [Board]
checkAllBoards bs ws = case checkBoards bs of
                         Nothing -> ws
                         Just b  -> checkAllBoards (filter (/=b) bs) (b:ws)

gameLoop :: [Board] -> [Int] -> Int
gameLoop boards (n:um) = do
  let game = markBoards n boards
  case checkBoards game of
    Nothing    -> gameLoop game um
    Just board -> boardSum board * n
gameLoop _ _ = 0

losingLoop :: [Board] -> [([Board], Int)]-> [Int] -> Int
losingLoop boards winners (n:um) = do
  let game = markBoards n boards
  case checkAllBoards game [] of
    []     -> losingLoop game winners um
    bs -> losingLoop (filter (`notElem` bs) game) ((bs, n):winners) um
losingLoop _ winners [] = do
  let boards = fst (head winners)
      num   = snd (head winners)
  head (map boardSum boards) * num


boardSum :: Board -> Int
boardSum board = sum $ map sumB board
  where
    sumB :: BingoRow -> Int
    sumB ((Unchecked x):xs) = x + sumB xs
    sumB (_:xs) = sumB xs
    sumB [] = 0
