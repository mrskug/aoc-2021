module Main where

import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.Hspec
import qualified Test.Tasty.Runners.Reporter as Reporter

import Dec2

main :: IO ()
main = defaultMainWithIngredients [Reporter.ingredient] unitTests

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "Part 1" $
      getPosFromCmds [Forward 5, Down 5, Forward 8, Up 3, Down 8, Forward 2] (0,0) `shouldBe` (15,10)
  , testCase "Part 2" $
      getPosFromCmdsWithAim [Forward 5, Down 5, Forward 8, Up 3, Down 8, Forward 2] (0,0,0) `shouldBe` (15,60,10)
  ]
