module Dec2 where


data Command = Up Int | Down Int | Forward Int
  deriving (Show, Read)

parseCommand :: String -> Command
parseCommand = read

getPosFromCmds :: [Command] -> (Int,Int) -> (Int, Int)
getPosFromCmds (x:xs) (hor,ver) = case x of
  Forward n -> getPosFromCmds xs (hor+n,ver)
  Up n      -> getPosFromCmds xs (hor,ver-n)
  Down n    -> getPosFromCmds xs (hor,ver+n)
getPosFromCmds [] pos = pos

getPosFromCmdsWithAim :: [Command] -> (Int,Int,Int) -> (Int, Int, Int)
getPosFromCmdsWithAim (x:xs) (hor,ver,aim) = case x of
  Forward n -> getPosFromCmdsWithAim xs (hor+n, ver+aim*n, aim)
  Up n      -> getPosFromCmdsWithAim xs (hor, ver, aim-n)
  Down n    -> getPosFromCmdsWithAim xs (hor, ver, aim+n)
getPosFromCmdsWithAim [] pos = pos
