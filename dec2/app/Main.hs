module Main where

import Data.Char (toUpper)

import Dec2

main :: IO ()
main = do
  input <- readFile "input.txt"
  let inputLines = lines input
      commands = map parseCommand (map (\(x:xs) -> (toUpper x : xs)) inputLines)
      (hor,ver) = getPosFromCmds commands (0,0)
      (horAim, verAim, _) = getPosFromCmdsWithAim commands (0,0,0)
  print $ (hor * ver, horAim * verAim)
