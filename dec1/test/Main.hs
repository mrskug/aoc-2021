module Main where

import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.Hspec
import qualified Test.Tasty.Runners.Reporter as Reporter

import Dec1

main :: IO ()
main = defaultMainWithIngredients [Reporter.ingredient] unitTests


unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "Part 1" $
      countIncreases 0 [199,200,208,210,200,207,240,269,260,263] `shouldBe` 7
  , testCase "Part 2" $
      countWindowIncreases 0 [199,200,208,210,200,207,240,269,260,263] `shouldBe` 5
  ]
