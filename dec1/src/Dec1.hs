module Dec1
    ( toInts
    , countIncreases
    , countWindowIncreases
    ) where

toInts :: [String] -> [Integer]
toInts = map read

countIncreases :: Int -> [Integer] -> Int
countIncreases acc (x:y:xs)
  | y > x     = countIncreases (acc + 1) (y:xs)
  | otherwise = countIncreases acc (y:xs)
countIncreases acc _ = acc

countWindowIncreases :: Int -> [Integer] -> Int
countWindowIncreases acc (a:b:c:d:xs)
  | b+c+d > a+b+c           = countWindowIncreases (acc+1) (b:c:d:xs)
  | otherwise               = countWindowIncreases acc (b:c:d:xs)
countWindowIncreases acc _ = acc
