module Main where

import Dec1

main :: IO ()
main = do
  input <- readFile "input.txt"
  let l = lines input
      depths = toInts l
  print (countIncreases 0 depths, countWindowIncreases 0 depths)
